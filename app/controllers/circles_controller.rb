class CirclesController < ApplicationController

  before_action :ensure_current_user, only: [:new, :create]

  def new
    @circle = Circle.new
  end

  def create
    @circle = current_user.circles.new(circle_params)

    if @circle.save
      redirect_to circle_url(@circle)
    else
      flash.now[:errors] = @circle.errors.full_messages
      render :new
    end

  end

  def edit
    @circle = Circle.find(params[:id])
    ensure_circle_owner
  end

  def update
    @circle = Circle.find(params[:id])
    ensure_circle_owner

    if @circle.update(circle_params)
      redirect_to circle_url(@circle)
    else
      flash.now[:errors] = @circle.errors.full_messages
      render :edit
    end
  end

  def index
    @circles = current_user.circles
  end


  def show
    @circle = Circle.find(params[:id])
  end

  private

  def circle_params
    params.require(:circle).permit(:name, member_ids: [])
  end

  def ensure_circle_owner
    redirect_to circle_url(@circle) unless current_user?(@circle.user)
  end

end
