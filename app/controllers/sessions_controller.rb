class SessionsController < ApplicationController

  skip_before_action :ensure_signed_in, only: [:new, :create]

  def new
  end

  def create
    @user = User.find_by_credentials(user_params)

    if @user
      login_user(@user)
      redirect_to root_url
    else
      flash.now[:errors] = "Wrong username or password"
      render :new
    end
  end

  def destroy
    current_user.reset_session_token!
    session[:token] = nil
    redirect_to new_session_url
  end

  private

  def user_params
    params.require(:user).permit(:email, :password)
  end
end
