class PostsController < ApplicationController

  before_action :ensure_current_user, only: [:new, :create]

  def new
    @post = Post.new
    3.times { @post.links.new }
  end

  def create
    @post = current_user.posts.new(post_params)
    @post.links.new(link_params)
    if @post.save
      redirect_to post_url(@post)
    else
      flash.now[:errors] = @post.errors.full_messages
      render :new
    end
  end

  def show
    @post = Post.find(params[:id])
  end

  def feed
    @posts = Post.joins(:viewers).where("circle_memberships.user_id = ?", current_user.id)
  end
  
  def index
    @posts = current_user.posts
  end

  private

  def post_params
    params.require(:post).permit(:body, :title, circle_ids: [])
  end

  def link_params
    params.permit(links: :url)
          .require(:links)
          .values
          .reject { |url| url.values.all?(&:blank?) }
  end
end
