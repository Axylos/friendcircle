class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :ensure_signed_in

  helper_method :current_user, :signed_in?, :current_user?

  def login_user(user)
    session[:token] = user.reset_session_token!
    @current_user = user
  end

  def current_user
    @current_user ||= User.find_by(session_token: session[:token])
  end

  def signed_in?
    !!current_user
  end

  def current_user?(user)
    current_user == user
  end


  private

  def ensure_signed_in
    redirect_to new_session_url unless signed_in?
  end

  def ensure_current_user
    unless current_user?(User.find(params[:user_id]))
      flash[:errors] = "Not You!"
      redirect_to user_url(current_user)
    end
  end
end
