class CircleMembership < ActiveRecord::Base
  validates :circle, :user_id, presence: true
  validates :user_id, uniqueness: { scope: :circle_id }

  belongs_to :circle
  belongs_to :user
end
