class Post < ActiveRecord::Base
  belongs_to :user
  has_many :links, inverse_of: :post
  has_many :post_shares, inverse_of: :post
  has_many :circles, through: :post_shares, source: :circle
  has_many :viewers, through: :circles, source: :members

  validates :user_id, :body, :title, presence: true
end
