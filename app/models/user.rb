class User < ActiveRecord::Base

  attr_reader :password

  #circle associations
  has_many :circles
  has_many :circle_members, through: :circles, source: :members

  #post associations
  has_many :posts

  #membership associations
  has_many :circle_memberships
  has_many :member_circles, through: :circle_memberships, source: :circle

  #validations
  validates :email, presence: true, uniqueness: true
  validates :password_digest, presence: { message: "Password can't be blank" }
  validates :session_token, presence: true
  validates :password, length: {minimum: 6, allow_nil: true}

  before_validation :ensure_session_token

  def password=(password)
    unless password.blank?
      @password = password
      self.password_digest = BCrypt::Password.create(password)
    end
  end

  def is_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end

  def self.find_by_credentials(params)
    user = User.find_by(email: params[:email])
    return nil if user.nil?
    return user if user.is_password?(params[:password])
  end

  def reset_session_token!
    self.session_token = User.generate_session_token
    self.save!
    return self.session_token
  end

  def self.generate_session_token
    SecureRandom.urlsafe_base64(16)
  end

  private
  def ensure_session_token
    self.session_token ||= User.generate_session_token
  end
end
