class Circle < ActiveRecord::Base
  validates :name, :user_id, presence: true
  validates :name, uniqueness: { scope: :user_id }

  belongs_to :user

  has_many :circle_memberships, inverse_of: :circle

  has_many :members, through: :circle_memberships, source: :user

  has_many :post_shares
  has_many :posts, through: :post_shares, source: :post
end
