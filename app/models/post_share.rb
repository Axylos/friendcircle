class PostShare < ActiveRecord::Base
  validates :circle_id, :post, presence: true

  belongs_to :circle
  belongs_to :post
end
